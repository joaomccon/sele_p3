#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "libs/lista_ligada.h"
#include "libs/common.h"
#include "libs/queue.h"
#include "libs/statistics.h"
#include "libs/histogram.h"
#include "libs/area_specific_operator.h"
#include "libs/general_purpose_operator.h"

/*
	TODO
		Falta implementar as estatisticas que pede 
		Avaliar se o que foi feito faz algum sentido
		Ele da core dumped quando o numero de area specific operators e baixo. Tal acontece porque ele tenta processar um evento que nao existe
*/

int main(int argc, char** argv)
{

	size_t general_purpose_number, area_specific_number, general_purpose_queue_size, number_events;
	size_t current_event = 0;

	if(argc != 5)
	{
		printf("p3 <number of general purpose operators> <number of area specific operators> <general purpose operators queue size> <number of clients requests>\n");
		return -1;
	}

	general_purpose_number = strtoul(argv[1], NULL, 10);
	area_specific_number = strtoul(argv[2], NULL, 10);
	general_purpose_queue_size = strtoul(argv[3], NULL, 10);
	number_events = strtoul(argv[4], NULL, 10);

	if(GeneralPurposeOperator.initialize(general_purpose_number, general_purpose_queue_size) || AreaSpecificOperator.initialize(area_specific_number, (size_t) -1))
		return -1;

	srand((unsigned)time(NULL));

	Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, ARRIVAL, 0.0));

	while(current_event < number_events)
	{

//printf("\n\nEVENT %ld ", current_event);
		if(Queue.eventTime(GeneralPurposeOperator.channelQ) < Queue.eventTime(AreaSpecificOperator.channelQ)) // Verifica qual das filas tem um evento que comeca primeiro
		{

			if(Queue.eventType(GeneralPurposeOperator.channelQ) == ARRIVAL)
			{
			//	printf("TIME %lf TYPE ARRIVAL GENERAL",Queue.eventTime(GeneralPurposeOperator.channelQ));
				GeneralPurposeOperator.eventArrival(NULL);
				current_event++;
				
			}
			else
			{
			//	printf("TIME %lf TYPE DEPARTURE GENERAL",Queue.eventTime(GeneralPurposeOperator.channelQ));
				GeneralPurposeOperator.eventDeparture();
			}
				
		}else{
		//	printf("TIME %lf TYPE DEPARTURE AREA",Queue.eventTime(AreaSpecificOperator.channelQ));
			AreaSpecificOperator.eventDeparture();
			
		}

	}

	printf("\nSTATISTICS\n");
	printf("Predicted delay: %lf\n", static_running_average(-1.0));
	printf("Actual average delay: %lf\n", Histogram.average(Histogram_delay));
	printf("Predicted delay absolute error: %lf\n", static_running_average(-1.0) - Histogram.average(Histogram_delay));
	printf("Predicted delay realtive error: %lf\n", (static_running_average(-1.0) - Histogram.average(Histogram_delay))/Histogram.average(Histogram_delay));
	printf("Packets lost: %ld\n", Statistics.getLostPackets());
	Histogram.export_csv(Histogram_predicted_delay_error, "predicted_error");

	return 0;
}
