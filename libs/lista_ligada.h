#ifndef LISTA_LIGADA_H
#define LISTA_LIGADA_H = 1

#include<stdio.h>
#include<stdlib.h>

#include"common.h"

// Defini��o da estrutura da lista
typedef struct lista_t{
	int tipo;
	double tempo;
	struct lista * proximo;
} lista;


// Fun��o que remove o primeiro elemento da lista
static lista * remover (lista * apontador)
{
	lista * lap;
	
	if(!apontador)
		return apontador;

	lap = (lista *)apontador -> proximo;
	free(apontador);
	return lap;
}

// Fun��o que adiciona novo elemento � lista, ordenando a mesma por tempo
static lista * adicionar (lista * apontador, int n_tipo, double n_tempo)
{
	lista * lap = apontador;
	lista * ap_aux, * ap_next;
	if(apontador == NULL)
	{
		apontador = (lista *) malloc(sizeof (lista));
		apontador -> proximo = NULL;
		apontador -> tipo = n_tipo;
		apontador -> tempo = n_tempo;
		return apontador;
	}
	else
	{
		if (apontador->tempo > n_tempo) {
	        ap_aux = (lista *) malloc(sizeof (lista));
	        ap_aux -> tipo = n_tipo;
            ap_aux -> tempo = n_tempo;
            ap_aux -> proximo = (struct lista *) apontador;
            return ap_aux;
	    }

		ap_next = (lista *)apontador -> proximo;
		while(apontador != NULL)
		{
			if((ap_next == NULL) || ((ap_next -> tempo) > n_tempo))
				break;
			apontador = (lista *)apontador -> proximo;
			ap_next = (lista *)apontador -> proximo;
		}
		ap_aux = (lista *)apontador -> proximo;
		apontador -> proximo = (struct lista *) malloc(sizeof (lista));
		apontador = (lista *)apontador -> proximo;
		if(ap_aux != NULL)
			apontador -> proximo = (struct lista *)ap_aux;
		else
			apontador -> proximo = NULL;
		apontador -> tipo = n_tipo;
		apontador -> tempo = n_tempo;
		return lap;
	}
}

// Fun��o que imprime no ecra todos os elementos da lista
static void imprimir (lista * apontador)
{
	if(apontador == NULL)
		printf("Lista vazia!\n");
	else
	{
		while(apontador != NULL)
		{
			printf("Tipo=%s\t\tTempo=%lf\n", apontador->tipo == ARRIVAL ? "ARRIVAL\t\t" : apontador->tipo == DEPARTURE_TERMINATE ? "DEPARTURE_TERMINATE" : "DEPARTURE_TRANSFER\t", apontador -> tempo);
			apontador = (lista *)apontador -> proximo;
		}
	}
}

static struct LinkedList
{
	lista * (*remove) (lista * linkedlist);
	lista * (*add) (lista * linkedlist, int new_type, double new_time);
	void (*print) (lista* linkedlist);

}LinkedList_struct;

struct LinkedList LinkedList = 
{
	.add = &adicionar,
	.remove = &remover,
	.print = &imprimir
};

#endif