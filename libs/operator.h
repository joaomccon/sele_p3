#ifndef OPERATOR_H
#define OPERATOR_H = 1

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "queue.h"
#include "lista_ligada.h"

struct Operator
{
	struct event_queue* waitingQ;
	struct event_queue* channelQ;
	
	int (*initialize)(size_t numberOfOperators, size_t queueSize);	
	int (*eventArrival)(lista* event);
	int (*eventDeparture)();
};

int Operator_initialize(struct Operator* new_operator, size_t numberOfOperators, size_t queueSize)
{

	new_operator->waitingQ = Queue.create(queueSize, ARRIVAL_FILL_INDICATOR);

	if(new_operator->waitingQ == NULL)
		return -1;

	new_operator->channelQ = Queue.create(numberOfOperators, DEPARTURE_FILL_INDICATOR);

	if(new_operator->channelQ == NULL)
		return -1;

	return 0;
}

#endif

