#ifndef GP_OPERATOR_H
#define GP_OPERATOR_H = 1

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "queue.h"
#include "lista_ligada.h"
#include "operator.h"
#include "area_specific_operator.h"

extern struct Operator AreaSpecificOperator;

static int GeneralPurposeOperator_initialize(size_t numberOfOperators, size_t queueSize);
static int GeneralPurposeOperator_eventArrival(lista* event);
static int GeneralPurposeOperator_eventDeparture();

#define CLIENT_ARRIVAL_RATE 80.0/3600.0

#define TRANSFER_AVG 1.0*60.0
#define TRANSFER_VAR 20.0
#define TRANSFER_MIN 30.0
#define TRANSFER_MAX 120.0

#define TERMINATE_AVG 2.0*60.0
#define TERMINATE_MIN 1.0*60.0
#define TERMINATE_MAX 5.0*60.0

#define HIST_DELAY_SIZE 512
#define HIST_DELAY_STEP 10

#define TERMINATE_PROBABILITY 0.3

size_t GeneralPurposeOperator_processedClients = 0;

struct histogram *Histogram_delay;
struct histogram *Histogram_predicted_delay_error;

struct Operator GeneralPurposeOperator =
{	
	.initialize = &GeneralPurposeOperator_initialize, 
	.eventArrival = &GeneralPurposeOperator_eventArrival,
	.eventDeparture = &GeneralPurposeOperator_eventDeparture
};

static int GeneralPurposeOperator_initialize(size_t numberOfOperators, size_t queueSize)
{
	if(Operator_initialize(&GeneralPurposeOperator, numberOfOperators, queueSize) < 0)
		return -1;
	
	Histogram_delay = Histogram.create(HIST_DELAY_SIZE, HIST_DELAY_STEP);	// Cria histograma para o delay verdadeiro dos clientes

	if(Histogram_delay == NULL)
		return -1;

	Histogram_predicted_delay_error = Histogram.create(HIST_DELAY_SIZE, HIST_DELAY_STEP);	// Cria histograma para os delays previstos para os clientes

	if(Histogram_predicted_delay_error == NULL)
		return -1;
	return 0;
}

static void GeneralPurposeOperator_eventDeparture_generate(double currentTime)
{

	switch(rand_normalizado() > TERMINATE_PROBABILITY ? DEPARTURE_TRANSFER : DEPARTURE_TERMINATE)	// Calcula se e para se transferir ou nao o cliente
	{
		case DEPARTURE_TERMINATE:
			Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TERMINATE, currentTime + exponential_distribution(TERMINATE_AVG, TERMINATE_MIN, TERMINATE_MAX)));
		break;

		case DEPARTURE_TRANSFER:

			Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TRANSFER, currentTime + gaussian_distribution_w_min_max(TRANSFER_AVG, TRANSFER_VAR, TRANSFER_MIN, TRANSFER_MAX)));

		break;
	};

}

// Caso o operador de caracter geral receba um cliente
static int GeneralPurposeOperator_eventArrival(lista* event)
{

	if(!Queue.isFull(GeneralPurposeOperator.channelQ))
	{

		GeneralPurposeOperator_eventDeparture_generate(Queue.eventTime(GeneralPurposeOperator.channelQ));	
		GeneralPurposeOperator_processedClients++;

	}else if(!Queue.isFull(GeneralPurposeOperator.waitingQ))
	{

		Queue.push(GeneralPurposeOperator.waitingQ, LinkedList.add(NULL, ARRIVAL, Queue.eventTime(GeneralPurposeOperator.channelQ)));
		
	}else
	{
		Statistics.lostPacket();
	}
	
	Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, ARRIVAL, poisson_distribution(CLIENT_ARRIVAL_RATE) + Queue.eventTime(GeneralPurposeOperator.channelQ)));	// Mete o novo cliente na fila para ser processado futuramente
	Queue.pop(GeneralPurposeOperator.channelQ);	// Tira o cliente que ja foi processada a sua chegada
	Histogram.add(Histogram_predicted_delay_error, static_running_average(-1)); // Guarda no histograma de previsao do tempo de espera o tempo previsto para este novo cliente

	return 0;
}

static void GeneralPurposeOperator_popWaitingQueue()
{
	if(!Queue.isEmpty(GeneralPurposeOperator.waitingQ))
		{	
			GeneralPurposeOperator_eventDeparture_generate(Queue.eventTime(GeneralPurposeOperator.channelQ));	// Mete o evento na fila dos operadores
			Histogram.add(Histogram_delay, Queue.eventTime(GeneralPurposeOperator.channelQ) - Queue.eventTime(GeneralPurposeOperator.waitingQ));	// Calcula o delay

			static_running_average(Queue.eventTime(GeneralPurposeOperator.channelQ) - Queue.eventTime(GeneralPurposeOperator.waitingQ));	// Calcula o delay esperado para atendimento do cliente
			Queue.pop(GeneralPurposeOperator.waitingQ);	// Tira o evento da fila de espera
		}
}

static int GeneralPurposeOperator_eventDeparture()
{
	
	switch(Queue.eventType(GeneralPurposeOperator.channelQ))
	{
		case DEPARTURE_TERMINATE:
		//	printf(" TERMINATE");
			GeneralPurposeOperator_popWaitingQueue();

		break;

		case DEPARTURE_TRANSFER:

		//	printf(" TRANSFER");

			if(!AreaSpecificOperator.eventArrival(LinkedList.add(NULL, ARRIVAL, Queue.eventTime(GeneralPurposeOperator.channelQ)))) // Passa o clietne para o specific area operator
				GeneralPurposeOperator.channelQ->waiting++; // Fica em espera ate o area specific atender o evento
			else
				GeneralPurposeOperator_popWaitingQueue();

		break;
	}
	
	Queue.pop(GeneralPurposeOperator.channelQ);

	return 0;
}

#endif