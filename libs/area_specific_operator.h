#ifndef AS_OPERATOR_H
#define AS_OPERATOR_H = 1

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "queue.h"
#include "lista_ligada.h"
#include "operator.h"
#include "general_purpose_operator.h"

#define AS_AVG 2.5 * 60.0
#define AS_MIN 1.0 * 60.0
#define AS_MAX __DBL_MAX__

extern struct Operator GeneralPurposeOperator;

static int AreaSpecificOperator_initialize(size_t numberOfOperators, size_t queueSize);
static int AreaSpecificOperator_eventArrival(lista* event);
static int AreaSpecificOperator_eventDeparture();

struct Operator AreaSpecificOperator =
{	
	.initialize = &AreaSpecificOperator_initialize, 
	.eventArrival = &AreaSpecificOperator_eventArrival,
	.eventDeparture = &AreaSpecificOperator_eventDeparture,
	.waitingQ = NULL,
	.channelQ = NULL
};

static int AreaSpecificOperator_initialize(size_t numberOfOperators, size_t queueSize)
{
	if(Operator_initialize(&AreaSpecificOperator, numberOfOperators, queueSize) < 0)
		return -1;

	return 0;
}

static int AreaSpecificOperator_eventArrival(lista* event)
{

	if(!Queue.isFull(AreaSpecificOperator.channelQ))	// Caso exista um operador de area livre
	{
		Queue.push(AreaSpecificOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TERMINATE, event->tempo + exponential_distribution(AS_AVG, AS_MIN, AS_MAX)) );	// Mete-se na fila dos operadores ocupados
		return 1;
	}else
	{
		Queue.push(AreaSpecificOperator.waitingQ, event);	// Mete-se na fila de espera
	}
	return 0;
}

static int AreaSpecificOperator_eventDeparture()
{
	
	size_t departureType = Queue.eventType(AreaSpecificOperator.channelQ);	// Guarda-se o tipo do evento em questao
	double departureTime = Queue.eventTime(AreaSpecificOperator.channelQ);	// Guarda-se o tempo do evento em questao
	
	Queue.pop(AreaSpecificOperator.channelQ);	// Tira-se o evento dos operadores ocupados

	if(!Queue.isEmpty(AreaSpecificOperator.waitingQ))	// Caso exista algum evento na fila de espera
	{	
		GeneralPurposeOperator.channelQ->waiting--;	// Permite que um dos eventos nos operadores de general purpose seja liberto
		Queue.push(AreaSpecificOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TERMINATE, departureTime + exponential_distribution(AS_AVG, AS_MIN, AS_MAX)));	// Mete-se o evento da fila de espera na fila dos operadores ocupados 
		Queue.pop(AreaSpecificOperator.waitingQ);	// Tira-se o evento da fila de espera
	}
	
	return 0;
}

#endif