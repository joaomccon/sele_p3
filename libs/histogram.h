#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define HISTOGRAMA_C_MAX 25
#define CSV_EXTENSION ".csv"

struct histogram
{
	size_t* valores;
	double step;
	size_t size;
	size_t total_samples;
};

static struct histogram* histogram_create (size_t size, double step)
{
	struct histogram* hist;

	if(step <= 0.0 || size <= 0)
		return NULL;
	
	hist = (struct histogram*)malloc(sizeof(struct histogram));

	if(!hist)
		return NULL;

	hist->valores = (size_t*)calloc(size, sizeof(size_t));

	if(!hist->valores)
		return NULL;

	hist->size = size;
	hist->step = step;
	hist->total_samples = 0;
	
	return hist;
}

static int histogram_adicionar (struct histogram* hist, double new_val)
{
	
	size_t index;

	if(!hist || new_val < 0.0)
		return -1;
	
	index = (size_t) round(new_val/hist->step);

	hist->valores[index >= hist->size ? hist->size - 1 : index]++;
	
	hist->total_samples++;

	return 0;
}

static void histogram_imprimir (struct histogram* hist)
{	
	size_t i,step;
	
	if(!hist)
		return;

	printf("\n*****HISTOGRAMA*****\n");

	for(step = 0; step < hist->size; step++)
	{
		printf("[%f]\t", (step+1)*hist->step);

		for(i = 0; i < hist->valores[step]; i++)
			printf("*");
		printf("\n");
	}

}

static double histogram_media(struct histogram* hist)
{
	
	double media = 0.0;
	size_t i;

	if(!hist)
		return -1.0;
	
	for(i = 0; i < hist->size; i++)
		media += hist->valores[i]*(hist->step*i)/hist->total_samples;
	
	return media;
}

static int histogram_exportar_csv(struct histogram* hist, char* filename)
{
	FILE *file;
	char filename_final[256];
	char buffer[256];

	size_t i;

	if(!hist || !filename)
		return -1;
	
	sprintf(filename_final, "csv/%s%s", filename, CSV_EXTENSION);

	file = fopen(filename_final, "w");

	if(!file)
	{
		printf("ERROR: can't export file %s.csv !!!\n", filename);
		return -1;
	}
		

	for(i = 0; i < hist->size; i++)
	{
		sprintf(buffer, "%f,%ld\n", hist->step * (i+1), hist->valores[i]);
		fwrite(buffer, strlen(buffer), 1, file);
	}
	
	fclose(file);
	return 0;
}	

static size_t histogram_get_number_of_samples(struct histogram* hist)
{
	return hist->total_samples;
}

static struct Histogram
{
	struct histogram* (*create) (size_t size, double step);
	int (*add) (struct histogram* hist, double new_val);
	double (*average)(struct histogram* hist);
	int (*export_csv)(struct histogram* hist, char* filename);
	void (*print) (struct histogram* hist);
	size_t (*getNumberOfSamples)(struct histogram* hist);
}Histograma_struct;

struct Histogram Histogram = 
{
	.create = &histogram_create,
	.add = &histogram_adicionar,
	.average = &histogram_media,
	.export_csv = &histogram_exportar_csv,
	.getNumberOfSamples = &histogram_get_number_of_samples,
	.print = &histogram_imprimir
};