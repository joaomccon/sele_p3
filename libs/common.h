#ifndef COMMON_H
#define COMMON_H = 1

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define delta(lambda) (double)( (1.0/(double)lambda) * 0.2)
#define media(total, n) (double)(total/n)
#define rand_normalizado()(double) (rand()/(RAND_MAX + 1.0)) // gera um random entre 0 e 1 (nunca exatamente 1)

#define PI 3.14159265358979323846

#define INVALID_TYPE 0
#define ARRIVAL 2
#define DEPARTURE_TERMINATE 1
#define DEPARTURE_TRANSFER 3

#define ARRIVAL_FILL_INDICATOR 2
#define DEPARTURE_FILL_INDICATOR 1

double gaussian_distribution_w_min_max(double average, double variance, double min, double max)
{
	double gaussian_dist = variance*(sqrt(-2 * log(rand_normalizado())) * cos(2*PI*rand_normalizado())) + average;

	return (gaussian_dist > max) ? max : ((gaussian_dist < min) ? min : gaussian_dist); 
}

double poisson_distribution(double lambda)
{
	return -1.0/(double) lambda* log(rand_normalizado());
}

double exponential_distribution(double average, double min, double max)
{
	double poisson = poisson_distribution(1/average);

	return (poisson > max) ? max :((poisson < min) ? min : poisson);
}

double static_running_average(double current_sample)
{
	static size_t number_of_samples = 0;
	static double current_average = 0.0;

	if(current_sample >= 0.0)
	{
		number_of_samples++;
		current_average = current_average * ((number_of_samples - 1)/ number_of_samples) + current_sample/(number_of_samples);
		return current_average;
	}

	return current_average;
}

#endif