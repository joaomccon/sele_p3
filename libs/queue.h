#ifndef QUEUE_H
#define QUEUE_H = 1

#include <stdlib.h>

struct event_queue
{	
	size_t max_size;
	size_t waiting;
	size_t fill_indicator;
	lista* events;
};

static struct event_queue* event_queue_create(size_t max_size, size_t fill_indicator)
{
	struct event_queue* q;

	q = (struct event_queue*)malloc(sizeof(struct event_queue));

	if(!q)
		return NULL;

	q->max_size = max_size;
	q->waiting = 0;
	q->events = NULL;
	q->fill_indicator = fill_indicator;

	return q;
}

static int event_queue_empty(struct event_queue* queue)
{	
	
	if(queue == NULL)
		return -1;

	if(!queue->waiting)
		return 1;
	
	return 0;
}

static int event_queue_full(struct event_queue* queue)
{
	if(queue == NULL)
		return -1;

	if(queue->events == NULL)
		return 0;

	if(queue->waiting == queue->max_size)
		return 1;
	
	return 0;
}

static lista* event_queue_pop(struct event_queue* queue)
{

	if((queue == NULL) || (event_queue_empty(queue)))
	{
		return NULL;
	}

	if(queue->events->tipo & queue->fill_indicator)
		queue->waiting--;

	queue->events = LinkedList.remove(queue->events);

	return queue->events;
}

static int event_queue_push(struct event_queue* queue, lista* new_event)
{

	if((queue == NULL) || (new_event == NULL) || (new_event->tipo == queue->fill_indicator && event_queue_full(queue)))
	{
		return -1;
	}

	queue->events = LinkedList.add(queue->events, new_event->tipo, new_event->tempo);
	
	if(new_event->tipo & queue->fill_indicator)
		queue->waiting++;

	return 0;
}

static int event_queue_type(struct event_queue* queue)
{
	if((queue == NULL))
	{
		return INVALID_TYPE;
	}

	return queue->events->tipo;
}

static double event_queue_time(struct event_queue* queue)
{
	if((queue == NULL) || (queue->events == NULL))
	{
		return __DBL_MAX__;
	}

	return queue->events->tempo;
}

static size_t event_queue_length(struct event_queue* queue)
{
	if((queue == NULL))
	{
		return -1;
	}

	return queue->waiting;

}

static struct Queue
{
	struct event_queue* (*create)(size_t max_size, size_t fill_indicator);
	lista* (*pop)(struct event_queue* queue);
	int (*push)(struct event_queue* queue, lista* new_event);
	int (*eventType)(struct event_queue* queue);
	double (*eventTime)(struct event_queue* queue);
	int (*isEmpty)(struct event_queue* queue);
	int (*isFull)(struct event_queue* queue);
	size_t (*length)(struct event_queue* queue);
}Queue_struct;

struct Queue Queue =
{
	.create = &event_queue_create,
	.pop = &event_queue_pop,
	.push = &event_queue_push,
	.eventType = &event_queue_type,
	.eventTime = &event_queue_time,
	.isEmpty = &event_queue_empty,
	.isFull = &event_queue_full,
	.length = &event_queue_length
};

#endif