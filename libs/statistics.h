#include <stdlib.h>

size_t total_packets;
size_t delayed_packets = 0;

static void statistics_lost_packet();
static size_t statistics_get_lost_packets();
static double statistics_lost_packets_percentage(double total);

static struct Statistics
{

    size_t packets_lost;

    void (*lostPacket)();
    size_t (*getLostPackets)();
    double (*lostPacketsPercentage)(double total);

}Statistics_struct;

struct Statistics Statistics = 
{
    .lostPacket = &statistics_lost_packet,
    .getLostPackets = &statistics_get_lost_packets,
    .lostPacketsPercentage = &statistics_lost_packets_percentage,
    .packets_lost = 0
};

static void statistics_lost_packet()
{
    Statistics.packets_lost++;
}

static size_t statistics_get_lost_packets()
{
    return Statistics.packets_lost;
}

static double statistics_lost_packets_percentage(double total)
{
    return Statistics.packets_lost/total;
}

