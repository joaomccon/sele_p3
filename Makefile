.DEFAULT_GOAL := instrucoes

instrucoes:
	@echo "COMANDOS COMPILACAO"
	@echo "	Para compilar: 					make compile"
	@echo "	Para limpar directoria:  			make clean"
	@echo "	Para simular um caso em particular:  		./p3 <number of general purpose operators> <number of area specific operators> <general purpose operators queue size> <number of clients requests>"

compile: p3.c
	@gcc p3.c -o p3 -lm

clean: 
	@rm -f p3
	@rm -f csv/*.csv

update_commit:
	@make clean
	@git add .
	@git commit -m "..."
	git push
	clear
